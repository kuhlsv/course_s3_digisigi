function [oldFrame, newFrame] = liveVideo(imScale, threshold, idKind)
% This function calls the needed functions to analyze a live video.
% 
% imScale is the coefficient for scaling the video.
% threshold is to handle small colour differences e.g. ignoring shadows.
% idKind identifies which type of movement is beeing analysed.

%% Get video
vid = selectCameraUI();
info = get(vid);
vHW = info.VideoResolution();
count = 0;
    
%% Get frames
endLive = true;
% Figure for output
f = figure('NumberTitle', 'off', 'Name', 'Live', 'Visible', 'on', 'Position', [300 200 600 500], 'CloseRequestFcn', @mclosereq2);
start(vid);
function mclosereq2(scr, evn, handles)
    endLive = false;
    stop(vid);
    delete(f);
    delete(vid);
    return
end
oldFrame = getsnapshot(vid);
newFrame = getsnapshot(vid);
while endLive % TODO: Safe loop term to exit needed
    I=getsnapshot(vid);
    oldFrame = newFrame;
    newFrame = I; %im2frame(I);
    [oldFrame, newFrame, count] = detectMotionL(oldFrame, newFrame, imScale, threshold, vHW, count, idKind);
    image(oldFrame);
end
waitfor(f);
return    
end
