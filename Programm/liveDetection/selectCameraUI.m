function vObj = selectCameraUI()
% This function displays a GUI enabling the user to choose which
% connected camera's feed will be analysed.
%
% returns a videoObject (Computer Vision System Toolbox)

%% create a GUI
f = figure('NumberTitle', 'off', 'Name', 'Select video', 'Visible', 'off', 'Position', [100 100 400 200], 'CloseRequestFcn', @mclosereq);
movegui(f, 'center');
b = uicontrol('Style', 'pushbutton', 'String', 'Search...', 'Position', [150 100 100 40], 'Callback', @picker);
%% select camera
function picker(s, e)
    % Video Object
    adapter = imaqhwinfo
    if(size(adapter.InstalledAdaptors) < 1)
        m = msgbox('No camera, webcam or Image Acquisition adaptors (addon)','Error');
        waitfor(m);
        return
    end
    vObj = videoinput('winvideo', 1); % Second param is the number of webcam to use (hardcoded)
    set(vObj, 'FramesPerTrigger', Inf);
    set(vObj, 'ReturnedColorspace', 'rgb');
    vObj.FrameGrabInterval = 1;  % distance between captured frames
    delete(f);
    return
    % TODO
end
%%
function mclosereq(scr, evn, handles)
    delete(f);
    return
end
%% Make figure visble after adding all components
f.Visible = 'on';
waitfor(f);
end

