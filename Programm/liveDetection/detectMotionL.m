function [oldFrame, frame, count] = detectMotion(oFrame, nFrame, imScale, threshold, vHW, count, idKind)
% This function detects motion in a live video.
% 
% oFrame is the last frame from the camera.
% nFrame is the new frame from the camera.
% vHW is the height and width of the video frames.
% count is the amount of movement.
% imScale is the coefficient for scaling the video.
% threshold is used to handle small colour differences e.g. ignoring shadows.
% idKind identifies which type of movement is beeing analysed.

%% serialize video
fPosition = [10 30];
fSize = 15;
vHeight = (vHW(2));
vWidth = (vHW(1));
    
%% Variables for analysis

% Number of total movement repetitions.
% Always shows the most counted movements inside all viewed squares.
maxMovementCount = 0;

% Percentage of needed change in colour to count as movement. 
% Detection of smaller movements requires lower value, 
% but also increases amount of false positives. 
%   default value: 0.7 
changeValueInPercent = 0.7;

% Side length of the square, where movement is observed.
movementDetectionSize = int32((vWidth * vHeight) / 1200); % TODO: replace 

% Amount of Pixels that are skipped between two pixels used for analysis
% TODO: replace 10000 by something relative to video resolution.
pixelSample = int32((vWidth * vHeight) / 100);

% Array length on x-scale
xScale = (vWidth) - (1+pixelSample);
xScale = int32(xScale / pixelSample) + 1;

% Array length on y-scale
yScale = (vHeight) - (1+pixelSample);
yScale = int32(yScale / pixelSample) + 1;

% Defines the maximum of how many movement repetitions can be counted.
%   default value: 1000
maxCountableMovement = 1000;

% Percentage of needed change in colour in the video to count as movement. 
% Detection of smaller movements requires lower value, 
% but also increases amount of false positives. 
%   default value: 3
percentage = 3;
minCountsForMovement = (xScale * yScale) * (percentage/100);

% Creates a matrix, which displays how many observed pixels show x-index
% amount of movements.
% e.g.: if 2 pixels counted 6 movements,
%       this matrix will have the value 2 at index 6
pixelAddedResult = zeros(maxCountableMovement);

movementsInObservedPixel = zeros(yScale,xScale);
%% get frames and modify
frame = nFrame;
% Saving the unchanged video
safeFrame = frame;
oldFrame = oFrame;
% Colour difference between frames.
frame = abs(frame - oldFrame);
    
for row = 1:pixelSample:size(frame, 1) % iterate through all rows
    for col = 1:pixelSample:size(frame, 2) % iterate through all cols
        if(frame(row,col,1) > threshold || frame(row,col,2) > threshold || frame(row,col,3) > threshold)
            frame(row,col,1) = 255; frame(row,col,2) = 255; frame(row,col,3) = 255;
        else
            frame(row,col,1) = 0; frame(row,col,2) = 0; frame(row,col,3) = 0;
        end

        % if we look at sourrounding pixels aswell, we have to check
        % first if we have space next to the pixel
        if(row + movementDetectionSize < size(frame,1) && col + movementDetectionSize < size(frame,2))
            rDiffer = double(abs(frame(row,col,1) - oldFrame(row,col,1)));
            colourDiffer = rDiffer;

            if(colourDiffer > 0)
                % if we have found a pixel with colourDifference we
                % will look at sourrounding pixels now
                differCount = 0;
                for extraX = 0:movementDetectionSize % x-index inside square
                    for extraY = 0:movementDetectionSize % y-index inside square
                        
                        newX = row + extraX; % x-index full frame
                        newY = col + extraY; % y-index full frame
                        % Check if any colour change exceeds the threshold.
                        if(frame(newX,newY,1) > threshold || frame(newX,newY,2) > threshold || frame(newX,newY,3) > threshold)
                            frame(newX,newY,1) = 255; frame(newX,newY,2) = 255; frame(newX,newY,3) = 255;
                        else
                            frame(newX,newY,1) = 0; frame(newX,newY,2) = 0; frame(newX,newY,3) = 0;
                        end
                        if(oldFrame(newX,newY,1) > threshold || oldFrame(newX,newY,2) > threshold || oldFrame(newX,newY,3) > threshold)
                            oldFrame(newX,newY,1) = 255; oldFrame(newX,newY,2) = 255; oldFrame(newX,newY,3) = 255;
                        else
                            oldFrame(newX,newY,1) = 0; oldFrame(newX,newY,2) = 0; oldFrame(newX,newY,3) = 0;
                        end
                        rDiffer = double(abs(frame(newX,newY,1) - oldFrame(newX,newY,1)));
                        colourDiffer = rDiffer;
                        if(colourDiffer > 0)
                            differCount = differCount + 1;
                        end
                    end
                end
                % Checks if the percentage of movement inside square
                % exceeds the changeSquareValuePercentage.
                if(differCount / (movementDetectionSize * movementDetectionSize) >= changeValueInPercent)
                    x = int32(row/pixelSample) + 1;
                    y = int32(col/pixelSample)+ 1;
                    % Add movement
                    movementsInObservedPixel(x,y) = movementsInObservedPixel(x,y)+ 1;
                    pixelAddedResult(movementsInObservedPixel(x,y)) = pixelAddedResult(movementsInObservedPixel(x,y)) + 1;
                    if(movementsInObservedPixel(x,y) > maxMovementCount && pixelAddedResult(movementsInObservedPixel(x,y)) > minCountsForMovement)
                        maxMovementCount = movementsInObservedPixel(x,y);
                    end
                end

            end
        end
    end
end
% Increment counter
count = count + maxMovementCount;
show = floor(count/2);
if idKind == 2
    show = count;
end
% Write to frame
oldFrame = insertText(safeFrame,fPosition,show,'FontSize',fSize,'AnchorPoint','LeftBottom'); % Add text, Computer Vision System Toolbox Addon
       
%% end
return

end

