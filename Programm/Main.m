% 07.01.2018
% Hausarbeit - Motion Detection
% Addons - (Video Object) Computer Vision System Toolbox, (Live video) Image Acquisition Toolbox, 
%          (Webcam drivers/adapter, other adapter possible) Image Acquisition Toolbox Support Package for OS Generic Video Interface 
% Sven, Ann-Kathrin, Fabian, Florian, Jan & Malte 
                    
%% Clear everything   

clear;
close all;
clc;

%% Initializing variables

% Add folders to search path.
addpath(genpath('liveDetection/'));
addpath(genpath('videoDetection/'));

% The threshold value refers to the needed change in colour to detect
% movement.
threshold = 40;

% Uses every nth frame.
%   default value: 4
samplingFactor = 4;

% The video is scaled by this amount.
imScale = 0.5;

% Dimensions of the output video
resizingParams = [540 720];

%% Window select mode

[mode, idKind] = selectModeUI();
if(mode == 1)
    [newMov, oldMov] = loadVideo(imScale, threshold, samplingFactor, resizingParams, idKind);
elseif(mode == 2)
    [newMov, oldMov] = liveVideo(imScale, threshold, idKind);
else
    return
end

%% Output

hf = figure;
set(hf, 'position', [200 200 resizingParams(2) resizingParams(1)]);
movie(hf, oldMov, 1, 15);
hg = figure;
set(hg, 'position', [200 200 resizingParams(2) resizingParams(1)]);
movie(hg, newMov, 1, 15);
close(hg)
