function [mode, idKind] = selectModeUI()
% This function displays a GUI offering the user the option to choose
% between analysing a recorded video or live feed from a webcam. The GUI
% also offers information on the needed addons.
%
% mode returns the selected mode:
%       loadVideo = 1
%       liveVideo = 2
% idKind returns the kind of movement:

%% Initializing variables

idKind = 0;
mode = 0;

%% Creating a GUI

f = figure('NumberTitle', 'off', 'Name', 'Select mode', 'Visible', 'off', 'Position', [100 100 300 300]);
movegui(f, 'center');
uicontrol('Style', 'pushbutton', 'String', 'Addons', 'Position', [250 280 50 20], 'Callback', @info);
uicontrol('Style', 'pushbutton', 'String', 'Load video', 'Position', [100 160 100 40], 'Callback', @set);
uicontrol('Style', 'pushbutton', 'String', 'Live video', 'Position', [100 100 100 40], 'Callback', @set);
kind = uicontrol('Style', 'popup', 'String', {'Dumbbell count (Repeating motion)', 'Detect a motion (Any motion)'}, 'Position', [50 200 200 40]);

%% Processing user input

function set(s, e)
    name = s.String;
    if(name == 'Load video')
        mode = 1;
    else
        mode = 2;
    end
    idKind = get(kind, 'Value');  % get input value
    close(f);
    return
end

%% Show addon info

function info(s, e)
    inf = figure('NumberTitle', 'off', 'Name', 'Addon info', 'Visible', 'off', 'Position', [100 100 500 300]);
    movegui(inf, 'center');
    uicontrol(inf, 'Style', 'text', 'String', {'Addons needed', ' ', ' ', '| Video Object |', ...
        'Computer Vision System Toolbox,', ' ', '| Live video |', 'Image Acquisition Toolbox,', ' ', ...
        '| Webcam drivers/adapter, may need other depending on camera model |', ... 
        'Image Acquisition Toolbox Support Package for OS Generic Video Interface '}, ...
        'Position', [50 50 400 200]);
    inf.Visible = 'on';
end

%% Make figure visble after adding all components

f.Visible = 'on';
waitfor(f);
end
