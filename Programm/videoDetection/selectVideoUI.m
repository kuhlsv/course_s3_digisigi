function file = selectVideoUI()
% This function displays a GUI allowing the user to choose a video from
% the filesystem.
%
% returns the selected file

%% Initializing variables

fileName = '';
pathName = '';
%% Create a GUI

f = figure('NumberTitle', 'off', 'Name', 'Select video', 'Visible', 'off', 'Position', [100 100 400 200], 'CloseRequestFcn', @mclosereq);
movegui(f, 'center');
b = uicontrol('Style', 'pushbutton', 'String', 'Browse...', 'Position', [150 100 100 40], 'Callback', @picker);
%% Select file

function picker(s, e)
    filterSpec = { '*.mp4','MP4-files (*.mp4)'; '*.avi','AVI-files (*.avi)'; };
    dialogTitle = 'Select video';
    [fileName,pathName] = uigetfile(filterSpec,dialogTitle);
    if isequal(fileName,0)
        picker(s, e);
    else
        delete(b);
        uicontrol('Style', 'text', 'String', fileName, 'Position', [120 100 150 40], 'FontSize', 14, 'ForegroundColor', 'red');
        uicontrol('Style', 'pushbutton', 'String', 'Select', 'Position', [150 50 100 40], 'Callback', @selected);
    end
end
%% return selected file

function selected(s, e)
    file = fullfile(pathName, fileName);
    delete(f);
    return
end
%% remove selected file

function mclosereq(scr, evn, handles)
    file = '';
    delete(f);
    return
end
%% make figure visible after adding all components

f.Visible = 'on';
waitfor(f);
end

