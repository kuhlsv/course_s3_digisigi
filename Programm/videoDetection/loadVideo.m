function [newMov, oldMov] = loadVideo(imScale, threshold, samplingFactor, resizingParams, idKind)
% This function calls the needed functions to analyze a recorded video.
% 
% imScale is the coefficient for scaling the video.
% threshold is to handle small colour differences like ignore shadows.
% samplingFactor reduces the amount of frames to process.
% resizingParams are the dimensions of the output video.
% idKind identifies which type of movement is beeing analysed.

%% Get video

file = selectVideoUI();
if(isempty(file))
    newMov = '';
    oldMov = '';
    return
end
vObj = VideoReader(file);

%% Detect motion

[newMov, oldMov] = detectMotionV(vObj, imScale, threshold, samplingFactor, resizingParams, idKind);
end
