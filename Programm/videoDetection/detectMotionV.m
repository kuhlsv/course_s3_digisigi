function [newMov, oldMov] = detectMotion(vObj, imScale, threshold, samplingFactor, resizingParams, idKind)
% This function detects motion in a recorded video.
% 
% vObj is a videoObject from the Computer Vision System Toolbox.
% imScale is the coefficient for scaling the video.
% threshold is to handle small colour differences e.g. ignoring shadows.
% samplingFactor reduces the amount of frames to process.
% resizingParams are the dimensions of the output video.
% idKind identifies which type of movement is beeing analysed.        

%% Serialize video
fPosition = [10 30];
fPosition2 = [10 50];
fSize = 10;
vDuration = vObj.Duration;
vFrames = floor(vObj.NumberOfFrame / samplingFactor);
vHeight = (vObj.Height)*imScale;
vWidth = (vObj.Width)*imScale;
newMov(1:vFrames) = struct('cdata', zeros(vHeight, vWidth, 3, 'uint8'), 'colormap', []);
oldMov(1:vFrames) = struct('cdata', zeros(vHeight, vWidth, 3, 'uint8'), 'colormap', []);
    
%% Variables for analysis

% Number of total movement repetitions.
% Always shows the most counted movements inside all viewed squares.
movementCounter = 0;   

% Side length of the square, where movement is observed.
movementDetectionSize = int32((vWidth * vHeight) / 120000);

% Percentage of needed change in colour inside a square to count as movement. 
% Detection of smaller movements requires lower value, 
% but also increases amount of false positives. 
%   default value: 0.7
changeSquareValuePercentage = 0.7;

% Amount of Pixels that are skipped between two pixels used for analysis
% TODO: replace 10000 by something relative to video resolution.
pixelSample = int32((vWidth * vHeight) / 10000); 

% Array length on x-scale
xScale = (vWidth) - (1+pixelSample);
xScale = int32(xScale / pixelSample) + 1;

% Array length on y-scale
yScale = (vHeight) - (1+pixelSample);
yScale = int32(yScale / pixelSample) + 1;

% Defines the maximum of how many movement repetitions can be counted.
%   default value: 1000
maxCountableMovement = 1000;

% Percentage of needed change in colour in the video to count as movement. 
% Detection of smaller movements requires lower value, 
% but also increases amount of false positives. 
%   default value: 0.03
changeTotalValuePercentage = 0.03; 
minCountsForMovement = (xScale * yScale) * (changeTotalValuePercentage);

% Creates a matrix, which displays how many observed pixels show x-index
% amount of movements.
% e.g.: if 2 pixels counted 6 movements,
%       this matrix will have the value 2 at index 6
pixelAddedResult = zeros(maxCountableMovement);

% Creates a matrix containing the currently observed pixels.
% Displays movement inside these pixels.
movementsInObservedPixel = zeros(yScale,xScale);

%% Get frames and modify

% Read all video frames
newFrames = [];
oldFrames = [];

% Resizing the video
frame = imresize(read(vObj, samplingFactor + 1),imScale);

% Saving the unchanged video
safeFrame = frame;

oldFrame = imresize(read(vObj, 1),imScale);
newFrames = cat(4, newFrames, frame);       % concatenate frames
oldFrames = cat(4, oldFrames, oldFrame);

fprintf('analysing....\n'); % indicates start of analysis in the console.

for k = 2 : vFrames
    
    oldFrame = safeFrame;
    frame = imresize(read(vObj, k * samplingFactor + 1),imScale);
    safeFrame = frame;
    
    % Colour difference between frames.
    frame = abs(frame - oldFrame);  
    
    for row = 1:pixelSample:size(frame, 1)      % iterate through all rows
        for col = 1:pixelSample:size(frame, 2)  % iterate through all cols
            
            % If the difference in colour exceeds the threshold parameter
            % a motion is detected.
            if(frame(row,col,1) > threshold || frame(row,col,2) > threshold || frame(row,col,3) > threshold)
                % Paint white to indicate movement.
                frame(row,col,1) = 255; frame(row,col,2) = 255; frame(row,col,3) = 255; 
            else
                % Paint black to indicate no movement.
                frame(row,col,1) = 0; frame(row,col,2) = 0; frame(row,col,3) = 0;
            end

            % Check if a square surrounding the observed pixel can be
            % created.
            if(row + movementDetectionSize < size(frame,1) && col + movementDetectionSize < size(frame,2))                
                % Difference in red colour between old and new frame.
                % Observing other colours is not needed. 
                colourDiffer = double(abs(frame(row,col,1) - oldFrame(row,col,1)));
                
                % If a pixel with a colour change is found, this will
                % look at the surrounding pixels in its square.
                if(colourDiffer > 0)
                    % Amount of changed pixels inside square.
                    differCount = 0;
                    
                    for extraX = 0:movementDetectionSize % x-index inside square
                        for extraY = 0:movementDetectionSize % y-index inside square

                            newX = row + extraX;    % x-index full frame
                            newY = col + extraY;    % y-index full frame
                            
                            % Check if any colour change exceeds the threshold.
                            if(frame(newX,newY,1) > threshold || frame(newX,newY,2) > threshold || frame(newX,newY,3) > threshold)
                                % Paint white to indicate movement.
                                frame(newX,newY,1) = 255; frame(newX,newY,2) = 255; frame(newX,newY,3) = 255;
                            else
                                % Paint black to indicate no movement.
                                frame(newX,newY,1) = 0; frame(newX,newY,2) = 0; frame(newX,newY,3) = 0;
                            end
                            if(frame(newX,newY,1) > 0)
                                % Add a movement to counter.
                                differCount = differCount + 1;
                            end
                        end
                    end
                    
                    % Checks if the percentage of movement inside square
                    % exceeds the changeSquareValuePercentage.
                    if(differCount / (movementDetectionSize * movementDetectionSize) >= changeSquareValuePercentage)
                        x = int32(row/pixelSample) + 1; % x-index of observed pixel
                        y = int32(col/pixelSample)+ 1;  % y-index of observed pixel
                        % Add movement
                        movementsInObservedPixel(x,y) = movementsInObservedPixel(x,y)+ 1;
                        pixelAddedResult(movementsInObservedPixel(x,y)) = pixelAddedResult(movementsInObservedPixel(x,y)) + 1;
                        % Check if the newly counted movements exceed the movementCounter
                        if(movementsInObservedPixel(x,y) > movementCounter && pixelAddedResult(movementsInObservedPixel(x,y)) > minCountsForMovement)
                            movementCounter = movementsInObservedPixel(x,y);
                        end
                    end
                end
            end
        end
    end
    %% Create the overlay displaying information
    
    % Process the selected kind of motion.
    % TODO: add more motion types and replace if with switch case.
    if idKind == 2
        % Any motion
        movements = floor(movementCounter); 
        vMText = strcat('Motion: ', num2str(movements));
    else
        % Repeating motion
        movements = floor(movementCounter/2);
        vMText = strcat('Count: ', num2str(movements));
    end
    % Write info into frame
    pastedDuration = (vDuration / vFrames) * k;
    vFrequenz = movements / pastedDuration;
    vFText = strcat('f (Hz): ', num2str(vFrequenz));
    oldFrame = insertText(safeFrame,fPosition2,vFText,'FontSize',fSize,'AnchorPoint','LeftBottom'); % Add text to frame
    oldFrame = insertText(oldFrame,fPosition,vMText,'FontSize',fSize,'AnchorPoint','LeftBottom'); % Add text to frame
    % Add to array
    oldFrames = cat(4, oldFrames, oldFrame);
    newFrames = cat(4, newFrames, frame); % 3=Greyscale, 4=Color, numel(size(thisFrame))+1=Check Dimension
end

%% Add an end frame to show result
% Process the selected kind of motion.
% TODO: add more motion types and replace if with switch case.
if idKind == 2
    % Any motion
    movements = floor(movementCounter);
    vMText = strcat('Motion: ', num2str(movements));
else
    % Repeating motion
    movements = floor(movementCounter/2);
    vMText = strcat('Count: ', num2str(movements));
end
% Write the result into last frame
vFrequenz = movements / vDuration;
vFText = strcat('f (Hz): ', num2str(vFrequenz));
oldFrame = insertText(255 * ones(vHeight, vWidth, 'uint8'),fPosition+[(0.45*vWidth) (0.45*vHeight)],vFText,'FontSize',fSize,'AnchorPoint','LeftBottom');
oldFrame = insertText(oldFrame,fPosition+[(0.45*vWidth) (0.55*vHeight)],vMText,'FontSize',fSize,'AnchorPoint','LeftBottom');
% Add last frame to array
oldFrames = cat(4, oldFrames, oldFrame);
newFrames = cat(4, newFrames, frame); % add last again
% Output
fprintf('finished analysing!\n');   % indicates end of analysis in the console.

%% Build video
for k = 1 : vFrames +1 % +1 for end frame
    newMov(k).cdata = imresize(newFrames(:,:,:,k), resizingParams);
    oldMov(k).cdata = imresize(oldFrames(:,:,:,k), resizingParams);
end

end
